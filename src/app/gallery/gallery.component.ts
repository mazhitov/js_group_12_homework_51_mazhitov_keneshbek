import {Component} from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent {
  password = '123456';
  passwordFromUser = '';
  showForm = false;
  isError = '';
  imageTitle = '';
  imageURL = '';
  images = [
    {imageTitle: 'muscleCar', imageURL: 'https://p4.wallpaperbetter.com/wallpaper/381/1008/63/dodge-classic-charger-muscle-car-hemi-hd-wallpaper-preview.jpg'},
    {imageTitle: 'chevrolet', imageURL: 'https://avatars.mds.yandex.net/get-autoru-vos/4792406/014cce65c3e4e2b26532145a0a1a58b0/456x342n'},
    {imageTitle: 'nissan gtr', imageURL: 'https://topgearrussia.ru/data/topgear/preview/2014-03/15/image-253ded0f-550x420.jpg'},
  ]
  onPullPasswordFromUser(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.passwordFromUser = target.value;
  }

  onCheckPasswordFromUser() {
    if (this.passwordFromUser === this.password) {
      this.showForm = true;
      this.passwordFromUser = '';
      this.isError = '';
    } else {
      this.passwordFromUser = '';
      this.isError = 'Your password incorrect.Please, input again!';
    }
  }

  onAddImage(event: Event) {
    event.preventDefault();
    this.images.push({imageTitle: this.imageTitle, imageURL: this.imageURL});
    this.imageTitle = '';
    this.imageURL = '';
  }
}
