import {Component} from '@angular/core';

@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styleUrls: ['./numbers.component.css']
})
export class NumbersComponent {
  randomNumbers: number[] = [];

  constructor() {
    this.generateNumbers();
  }

  generateNumbers() {
    this.randomNumbers = [];
    while (this.randomNumbers.length < 5){
      const randomNumber = Math.floor(Math.random() * (36 - 5)) + 5;
      if(this.randomNumbers.indexOf(randomNumber) === -1) this.randomNumbers.push(randomNumber);
    }
  }
}
